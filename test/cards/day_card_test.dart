import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:qyre_test/components/atoms/day_card.dart';

void main() {
  testWidgets(
    'Day card',
    (tester) async {
      await tester.pumpWidget(
        const MaterialApp(
          home: Scaffold(
            body: DayCard(
              title: 'title',
              subTitle: 'subTitle',
              label: 'label',
              dayLabel: 'dayLabel',
              type: DayCardType.red,
            ),
          ),
        ),
      );
      final titleFinder = find.text('title');
      final subTitleFinder = find.text('subTitle');
      final labelFinder = find.text('label');
      final dayLabelFinder = find.text('dayLabel');

      expect(titleFinder, findsOneWidget);
      expect(subTitleFinder, findsOneWidget);
      expect(labelFinder, findsOneWidget);
      expect(dayLabelFinder, findsOneWidget);
      expect(find.byKey(const Key("colored_button")), findsOneWidget);
      // final typeFinder = find.text(text)
    },
  );
}
