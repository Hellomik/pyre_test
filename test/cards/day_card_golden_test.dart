import 'package:flutter/material.dart';
import 'package:golden_toolkit/golden_toolkit.dart';
import 'package:qyre_test/components/atoms/day_card.dart';

void main() {
  testGoldens("Day Card Golden Test", (tester) async {
    await loadAppFonts();
    final builder = DeviceBuilder()
      ..overrideDevicesForAllScenarios(
        devices: [
          Device.phone,
          Device.iphone11,
          Device.tabletPortrait,
          Device.tabletLandscape,
        ],
      )
      ..addScenario(
        name: 'label',
        widget: Builder(builder: (context) {
          return const Center(
            child: DayCard(
              dayLabel: 'day label',
              label: 'label',
            ),
          );
        }),
      );
    await tester.pumpDeviceBuilder(builder, wrapper: (Widget child) {
      return MaterialApp(
        home: Scaffold(
          body: child,
        ),
      );
    });

    await screenMatchesGolden(tester, 'DayCard');
  });
}
