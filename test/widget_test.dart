import 'package:flutter_test/flutter_test.dart';
import 'package:qyre_test/domain/production/entities/production.entity.dart';

void main() {
  test('Counter value should be incremented', () {
    final prd = ProductionEntity(
      country: 'Sweden',
      title: 'Production Name That is Long',
      startDateTime: DateTime(2022, 1, 14),
      endDateTime: DateTime(2023, 4, 23),
      imageUrl: 'https://pbs.twimg.com/profile_images/839881982574108672/qN7BVlr4_400x400.jpg',
    );

    expect(prd.country, 'Sweden');
  });
}
