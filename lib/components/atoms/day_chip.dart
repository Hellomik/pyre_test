import 'package:flutter/material.dart';
import 'package:qyre_test/components/atoms/day_card.dart';
import 'package:qyre_test/utils/color.dart';

class DayChip extends StatelessWidget {
  final String? dayLabel;
  final String weekLabel;
  final String dateLabel;
  final DayCardType? type;

  const DayChip({
    this.dayLabel,
    required this.weekLabel,
    required this.dateLabel,
    this.type,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 36,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        color: ColorData.n1Black100,
      ),
      padding: const EdgeInsets.symmetric(horizontal: 11),
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              if (dayLabel != null)
                Padding(
                  padding: const EdgeInsets.only(right: 2),
                  child: Text(
                    dayLabel!,
                    style: const TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 10,
                      height: 13 / 10,
                      color: ColorData.n6Gray100,
                    ),
                  ),
                ),
              Text(
                weekLabel,
                style: const TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 10,
                  height: 10 / 10,
                  color: ColorData.n10WhiteNumber100,
                ),
              ),
            ],
          ),
          const SizedBox(height: 2),
          Row(
            children: [
              if (type != null)
                Container(
                  margin: const EdgeInsets.only(right: 2),
                  decoration: ShapeDecoration(shape: const CircleBorder(), color: type!.color),
                  height: 8,
                  width: 8,
                ),
              Text(
                dateLabel,
                style: const TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 10,
                  height: 10 / 10,
                  color: ColorData.n10WhiteNumber100,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
