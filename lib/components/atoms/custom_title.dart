import 'package:flutter/material.dart';
import 'package:qyre_test/utils/color.dart';

class CustomTitle extends StatelessWidget {
  final String title;

  const CustomTitle({
    required this.title,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 44,
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(
          title,
          style: const TextStyle(
            color: ColorData.n3Black50,
            fontSize: 18,
            height: 18 / 18,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}
