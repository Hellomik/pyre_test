import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:qyre_test/icons/qyre_small_arrow_right.painter.dart';
import 'package:qyre_test/utils/color.dart';

class ProductionCard extends StatelessWidget {
  final String country;
  final String title;
  final DateTime startDateTime;
  final DateTime endDateTime;
  final String imageUrl;

  const ProductionCard({
    required this.country,
    required this.title,
    required this.startDateTime,
    required this.endDateTime,
    required this.imageUrl,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: ColorData.n9Gray25,
        borderRadius: BorderRadius.circular(4),
      ),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: const BorderRadius.horizontal(left: Radius.circular(4)),
            child: Image.network(
              imageUrl,
              height: 70,
              width: 70,
              fit: BoxFit.cover,
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 16),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 4.0),
                          child: Text(
                            title,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                              color: ColorData.n1Black100,
                              fontWeight: FontWeight.w600,
                              fontSize: 14,
                              height: 16.8 / 14,
                            ),
                          ),
                        ),
                        Wrap(
                          children: [
                            Text(
                              country,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                color: ColorData.n5Black10,
                                fontWeight: FontWeight.w400,
                                fontSize: 10,
                                height: 9 / 10,
                              ),
                            ),
                            const SizedBox(width: 6),
                            Text(
                              "${DateFormat("MMM dd, yyyy").format(startDateTime)}"
                              " - "
                              "${DateFormat("MMM dd, yyyy").format(endDateTime)}",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                color: ColorData.n5Black10,
                                fontWeight: FontWeight.w400,
                                fontSize: 10,
                                height: 9 / 10,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(left: 6),
                    child: CustomPaint(
                      painter: QyreSmallArrowRightPainter(
                        color: ColorData.n1Black100,
                      ),
                      size: Size(10, 10),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
