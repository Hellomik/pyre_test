import 'package:flutter/material.dart';
import 'package:qyre_test/icons/qyre_small_arrow_right.painter.dart';
import 'package:qyre_test/utils/color.dart';
import 'package:qyre_test/utils/get_effective_text_style.dart';

class SuggestCard extends StatelessWidget {
  final String? title;
  final num? percentage;
  final String? label;

  const SuggestCard({
    this.title,
    this.percentage,
    this.label,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      decoration: BoxDecoration(
        color: ColorData.n9Gray25,
        borderRadius: BorderRadius.circular(4),
      ),
      padding: const EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 20),
      child: Column(
        children: [
          Expanded(
            child: Column(
              children: [
                if (title != null)
                  Padding(
                    padding: const EdgeInsets.only(bottom: 18),
                    child: Text(
                      title!,
                      style: const TextStyle(
                        color: ColorData.n1Black100,
                        fontWeight: FontWeight.w700,
                        fontSize: 16,
                        height: 19.2 / 16,
                      ),
                    ),
                  ),
                if (percentage != null)
                  SizedBox(
                    height: 9,
                    width: double.infinity,
                    child: CustomPaint(
                      painter: PercentageBoxPainter(percentage: percentage!),
                    ),
                  )
              ],
            ),
          ),
          if (label != null)
            Align(
              alignment: Alignment.centerRight,
              child: RichText(
                text: TextSpan(
                  style: getEffectiveTextStyle(
                    style: const TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      height: 16.8 / 14,
                      color: ColorData.n1Black100,
                    ),
                    context: context,
                  ),
                  children: [
                    TextSpan(
                      text: label,
                    ),
                    const WidgetSpan(
                      alignment: PlaceholderAlignment.middle,
                      child: Padding(
                        padding: EdgeInsets.only(left: 4),
                        child: CustomPaint(
                          size: Size(10, 10),
                          painter: QyreSmallArrowRightPainter(
                            color: ColorData.n1Black100,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )
        ],
      ),
    );
  }
}

class PercentageBoxPainter extends CustomPainter {
  final num percentage;
  final Color color;
  final Color backgroundColor;

  const PercentageBoxPainter({
    this.percentage = 0,
    this.backgroundColor = ColorData.n10WhiteNumber100,
    this.color = ColorData.n12BlueFaded,
    super.repaint,
  });
  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawRect(
      Rect.fromLTRB(
        0,
        0,
        size.width,
        size.height,
      ),
      Paint()..color = backgroundColor,
    );

    canvas.drawRect(
      Rect.fromLTRB(
        0,
        0,
        size.width * percentage,
        size.height,
      ),
      Paint()..color = color,
    );
  }

  @override
  bool shouldRepaint(covariant PercentageBoxPainter oldDelegate) {
    return oldDelegate.percentage != percentage || oldDelegate.color != color;
  }
}
