import 'package:flutter/material.dart';

import 'package:qyre_test/icons/qyre_attached_picture_line.dart';
import 'package:qyre_test/icons/qyre_pinned_line.painter.dart';
import 'package:qyre_test/icons/qyre_poll_line.painter.dart';
import 'package:qyre_test/utils/color.dart';

class StarredCard extends StatelessWidget {
  final String title;
  final String subTitle;
  final Duration duration;
  final String description;

  const StarredCard({
    required this.title,
    required this.subTitle,
    required this.duration,
    required this.description,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 16,
        vertical: 10,
      ),
      decoration: BoxDecoration(
        color: ColorData.n9Gray25,
        borderRadius: BorderRadius.circular(9),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: Text(
                  title,
                  style: const TextStyle(
                    fontSize: 16,
                    height: 19 / 16,
                    color: ColorData.n3Black50,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              Text(
                "${duration.inDays} day ago",
                style: const TextStyle(
                  fontSize: 12,
                  height: 15.6 / 12,
                  color: ColorData.n5Black10,
                  fontWeight: FontWeight.w400,
                ),
                textAlign: TextAlign.right,
              ),
            ],
          ),
          const SizedBox(height: 8),
          const Divider(
            height: 1,
            color: ColorData.n10WhiteNumber100,
          ),
          const SizedBox(height: 8),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Text(
                  subTitle,
                  style: const TextStyle(
                    fontSize: 14,
                    height: 16.8 / 14,
                    color: ColorData.n1Black100,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              const SizedBox(width: 6),
              Row(
                children: const [
                  CustomPaint(
                    painter: QyrePollLinePainter(color: ColorData.n12BlueFaded),
                    size: Size(24, 24),
                  ),
                  SizedBox(width: 6),
                  CustomPaint(
                    painter: QyreAttachedPciturePainter(color: ColorData.n12BlueFaded),
                    size: Size(24, 24),
                  ),
                  SizedBox(width: 6),
                  CustomPaint(
                    painter: QyrePinnedLinePainter(color: ColorData.n12BlueFaded),
                    size: Size(24, 24),
                  ),
                ],
              )
            ],
          ),
          const SizedBox(height: 8),
          Text(
            description,
            style: const TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 15,
              height: 19.5 / 15,
              color: ColorData.n5Black10,
            ),
          )
        ],
      ),
    );
  }
}
