import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:qyre_test/utils/color.dart';

class JobCard extends StatelessWidget {
  final String title;
  final DateTime initDate;
  final String subTitle;
  final DateTime startDate;
  final DateTime endDate;
  final Duration duration;

  const JobCard({
    required this.title,
    required this.initDate,
    required this.subTitle,
    required this.startDate,
    required this.endDate,
    required this.duration,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 16,
        vertical: 10,
      ),
      decoration: BoxDecoration(
        color: ColorData.n9Gray25,
        borderRadius: BorderRadius.circular(9),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: Text(
                  title,
                  style: const TextStyle(
                    fontSize: 16,
                    height: 19 / 16,
                    color: ColorData.n3Black50,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              Text(
                DateFormat('MMM dd, yyyy').format(initDate),
                style: const TextStyle(
                  fontSize: 12,
                  height: 15.6 / 12,
                  color: ColorData.n5Black10,
                  fontWeight: FontWeight.w400,
                ),
                textAlign: TextAlign.right,
              ),
            ],
          ),
          const SizedBox(height: 8),
          const Divider(
            height: 1,
            color: ColorData.n10WhiteNumber100,
          ),
          const SizedBox(height: 8),
          Text(
            subTitle,
            style: const TextStyle(
              fontSize: 14,
              height: 16.8 / 14,
              color: ColorData.n1Black100,
              fontWeight: FontWeight.w500,
            ),
          ),
          const SizedBox(height: 5),
          Row(
            children: [
              Expanded(
                child: Text(
                  "${DateFormat('MMM dd, yyyy').format(startDate)}"
                  " - "
                  "${DateFormat('MMM dd, yyyy').format(endDate)}",
                  style: const TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 12,
                    height: 15.6 / 12,
                  ),
                ),
              ),
              Text(
                "${duration.inDays}"
                ' days',
                style: const TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 12,
                  height: 15.6 / 12,
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
