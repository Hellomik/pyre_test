import 'package:flutter/material.dart';
import 'package:qyre_test/utils/color.dart';

enum DayCardType {
  red(color: ColorData.n18RedBright),
  blue(color: ColorData.n12BlueFaded);

  final Color color;
  const DayCardType({required this.color});
}

class DayCard extends StatelessWidget {
  final String? title;
  final String? subTitle;
  final String? label;
  final String? dayLabel;
  final DayCardType? type;

  const DayCard({
    this.title,
    this.subTitle,
    this.label,
    this.dayLabel,
    this.type,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: IntrinsicHeight(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: ColorData.n1Black100,
          ),
          width: 62,
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
          child: Column(
            children: [
              if (title != null)
                Padding(
                  padding: const EdgeInsets.only(bottom: 4.0),
                  child: Text(
                    title!,
                    style: const TextStyle(
                      color: ColorData.n6Gray100,
                      fontSize: 10,
                      fontWeight: FontWeight.w600,
                      height: 13 / 10,
                    ),
                    maxLines: 1,
                  ),
                ),
              if (subTitle != null)
                Padding(
                  padding: const EdgeInsets.only(bottom: 2.0),
                  child: Text(
                    subTitle!,
                    style: const TextStyle(
                      color: ColorData.n10WhiteNumber100,
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                      height: 15.6 / 12,
                    ),
                    maxLines: 1,
                  ),
                ),
              if (label != null)
                Padding(
                  padding: const EdgeInsets.only(bottom: 2.0),
                  child: Text(
                    label!,
                    style: const TextStyle(
                      color: ColorData.n10WhiteNumber100,
                      fontSize: 10,
                      fontWeight: FontWeight.w600,
                      height: 10 / 10,
                    ),
                    maxLines: 1,
                  ),
                ),
              if (dayLabel != null)
                Text(
                  dayLabel!,
                  style: const TextStyle(
                    color: ColorData.n10WhiteNumber100,
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                    height: 19.2 / 16,
                  ),
                  maxLines: 1,
                ),
              if (type != null)
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: CircleAvatar(
                    key: const Key('colored_button'),
                    backgroundColor: type!.color,
                    radius: 4,
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
