import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:qyre_test/components/atoms/day_card.dart';
import 'package:qyre_test/components/atoms/day_chip.dart';
import 'package:qyre_test/utils/color.dart';
import 'dart:math' as math;

class CustomAppBar extends StatefulWidget {
  final String? title;
  final ScrollController scrollController;

  const CustomAppBar({
    this.title,
    required this.scrollController,
    super.key,
  });

  @override
  State<CustomAppBar> createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> with SingleTickerProviderStateMixin {
  late AnimationController? animationController = AnimationController(
    vsync: this,
    duration: const Duration(milliseconds: 240),
    lowerBound: 0,
    upperBound: 1,
  );
  late final mediaQuery = MediaQuery.of(context);
  late final Animation<double> animationSize = CurvedAnimation(
    parent: animationController!,
    curve: Curves.linear,
  );

  late final Animation<Offset> animationOffset = Tween<Offset>(
    begin: const Offset(0, -.2),
    end: const Offset(0, 0),
  ).animate(
    CurvedAnimation(
      parent: animationController!,
      curve: Curves.linear,
    ),
  );
  @override
  void initState() {
    widget.scrollController.addListener(_listenScroll);
    super.initState();
  }

  @override
  void dispose() {
    widget.scrollController.removeListener(_listenScroll);
    animationController?.dispose();
    animationController = null;
    super.dispose();
  }

  final initDate = DateTime.now();

  _listenScroll() {
    if (widget.scrollController.hasClients) {
      if (widget.scrollController.offset > mediaQuery.padding.top + 54 - 20) {
        if (animationController?.status != AnimationStatus.forward) animationController?.forward();
      } else {
        if (animationController?.status != AnimationStatus.reverse) animationController?.reverse();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final random = math.Random();
    final Widget? child;
    if (widget.title != null) {
      child = Align(
        alignment: Alignment.centerLeft,
        child: Text(
          widget.title!,
          style: const TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w600,
            height: 18 / 18,
            color: ColorData.n3Black50,
          ),
        ),
      );
    } else {
      child = null;
    }

    return Material(
      color: Colors.white.withOpacity(0.7),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(
              top: mediaQuery.padding.top + 5,
              bottom: 5,
              left: 16,
              right: 16,
            ),
            child: SizedBox(
              height: 44,
              child: child,
            ),
          ),
          // AnimatedSize(duration: duration)
          Stack(
            clipBehavior: Clip.none,
            children: [
              SizeTransition(
                sizeFactor: animationSize,
                axisAlignment: 1,
                child: const SizedBox(height: 48),
              ),
              Positioned(
                left: 0,
                bottom: 0,
                right: 0,
                height: 46,
                child: Align(
                  alignment: Alignment.topCenter,
                  child: FadeTransition(
                    opacity: animationController!,
                    child: SlideTransition(
                      position: animationOffset,
                      child: SizedBox(
                        height: 36,
                        child: ListView.separated(
                          scrollDirection: Axis.horizontal,
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          itemBuilder: (ctx, index) {
                            final date = initDate.add(Duration(days: index));
                            DayCardType? dayCardType;

                            final ind = random.nextInt(3);
                            if (ind == 1) {
                              dayCardType = DayCardType.red;
                            } else if (ind == 2) {
                              dayCardType = DayCardType.blue;
                            }
                            return DayChip(
                              dateLabel: DateFormat("dd MMM.").format(date),
                              weekLabel: DateFormat("EEE").format(date),
                              dayLabel: index != 0 ? null : "TODAY",
                              type: dayCardType,
                            );
                          },
                          separatorBuilder: (context, index) => const SizedBox(width: 8),
                          itemCount: 7,
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
