import 'package:flutter/material.dart';

import 'package:qyre_test/icons/production_picture.painter.dart';

import 'package:qyre_test/components/atoms/custom_title.dart';
import 'package:qyre_test/components/molecules/empty_body.dart';
import 'package:qyre_test/components/atoms/production_card.dart';
import 'package:qyre_test/domain/production/entities/production.entity.dart';

class ProductionBody extends StatelessWidget {
  final List<ProductionEntity> productions;

  const ProductionBody({
    this.productions = const [],
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 44,
            child: CustomTitle(title: 'Today’s productions'),
          ),
          if (productions.isEmpty)
            const EmptyBody(
              painter: ProductionPainterPainter(),
              painterSize: Size(54, 54),
              title: 'All of your today’s productions will be displayed here.',
            ),
          for (int i = 0; i < productions.length * 2 - 1; ++i)
            if (i.isOdd)
              const SizedBox(height: 10)
            else
              ProductionCard(
                country: productions[i ~/ 2].country,
                title: productions[i ~/ 2].title,
                startDateTime: productions[i ~/ 2].startDateTime,
                endDateTime: productions[i ~/ 2].endDateTime,
                imageUrl: productions[i ~/ 2].imageUrl,
              ),
        ],
      ),
    );
  }
}
