import 'package:flutter/material.dart';
import 'package:qyre_test/icons/starred_post_picutre.painter.dart';
import 'package:qyre_test/components/atoms/custom_title.dart';
import 'package:qyre_test/components/molecules/empty_body.dart';
import 'package:qyre_test/components/atoms/starred_card.dart';
import 'package:qyre_test/domain/starred/entities/starred.entity.dart';

class StarredBody extends StatelessWidget {
  final List<StarredEntity> starred;

  const StarredBody({this.starred = const [], super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 44,
            child: CustomTitle(title: 'Starred posts'),
          ),
          if (starred.isEmpty)
            const EmptyBody(
              painter: StarredPostPicutrePainter(),
              painterSize: Size(54, 54),
              title:
                  'Posts that are extra relevant to you can be marked with a star and will be shown here for easy access.',
            ),
          for (int i = 0; i < starred.length * 2 - 1; ++i)
            if (i.isOdd)
              const SizedBox(height: 10)
            else
              StarredCard(
                title: starred[i ~/ 2].title,
                subTitle: starred[i ~/ 2].subTitle,
                duration: starred[i ~/ 2].duration,
                description: starred[i ~/ 2].description,
              ),
        ],
      ),
    );
  }
}
