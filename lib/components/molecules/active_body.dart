import 'package:flutter/material.dart';
import 'package:qyre_test/icons/my_network.painter.dart';
import 'package:qyre_test/icons/quick_hire.painter.dart';
import 'package:qyre_test/icons/subtract.painter.dart';
import 'package:qyre_test/utils/color.dart';
import 'package:qyre_test/utils/get_linear_gradient_alignment.dart';

class ActiveBody extends StatelessWidget {
  const ActiveBody({super.key});

  Widget getCard({
    required LinearGradient gradient,
    required String title,
    required String subTitle,
    required CustomPainter painter,
    Size painterSize = const Size(32, 32),
  }) {
    return Container(
      height: 140,
      decoration: BoxDecoration(
        gradient: gradient,
        borderRadius: BorderRadius.circular(4),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 17),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomPaint(
            size: painterSize,
            painter: painter,
          ),
          const SizedBox(height: 12),
          Text(
            title,
            style: const TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 14,
              height: 17 / 14,
              color: ColorData.n10WhiteNumber100,
            ),
          ),
          const SizedBox(height: 3),
          Text(
            subTitle,
            style: const TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 10,
              height: 12 / 10,
              color: ColorData.n10WhiteNumber100,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 16,
        vertical: 10,
      ),
      child: Row(
        children: [
          Expanded(
            child: getCard(
              gradient: LinearGradient(
                colors: ColorData.n20GradientBlueGradientColors.colors,
                begin: -getEndAlignment(151.66),
                end: getEndAlignment(151.66),
                stops: const [
                  7.72 / 100,
                  99.2 / 100,
                ],
              ),
              title: 'My network',
              subTitle: 'Connect and grow your network',
              painter: const MyNetworkPainter(
                color: ColorData.n10WhiteNumber100,
              ),
              painterSize: const Size(50, 34),
            ),
          ),
          // MyNetworkPainter
          const SizedBox(width: 10),
          Expanded(
            child: getCard(
              gradient: LinearGradient(
                colors: ColorData.n21GradientRedColors.colors,
                begin: -getEndAlignment(153.74),
                end: getEndAlignment(153.74),
                stops: const [
                  7.72 / 100,
                  99.2 / 100,
                ],
              ),
              title: 'Quick hire',
              subTitle: 'Hire someone quickly today',
              painter: const QuickHirePainter(color: ColorData.n10WhiteNumber100),
            ),
          ),
          const SizedBox(width: 10),
          Expanded(
            child: getCard(
              gradient: LinearGradient(
                colors: ColorData.n22GradientPurpleColors.colors,
                begin: -getEndAlignment(156.93),
                end: getEndAlignment(156.93),
                stops: const [
                  7.72 / 100,
                  99.2 / 100,
                ],
              ),
              title: 'My CV',
              subTitle: 'Kepp your CV updated to get the best offers',
              painter: const SubtractPainter(color: ColorData.n10WhiteNumber100),
            ),
          ),
        ],
      ),
    );
  }
}
