import 'package:flutter/material.dart';
import 'package:qyre_test/utils/color.dart';

class EmptyBody extends StatelessWidget {
  final CustomPainter painter;
  final Size painterSize;
  final String title;
  const EmptyBody({
    required this.painter,
    this.painterSize = const Size(54, 54),
    required this.title,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        color: ColorData.n9Gray25,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 16),
            child: CustomPaint(
              size: painterSize,
              painter: painter,
            ),
          ),
          Expanded(
            child: Text(
              title,
              style: const TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 15,
                height: 19.5 / 15,
                color: ColorData.n5Black10,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
