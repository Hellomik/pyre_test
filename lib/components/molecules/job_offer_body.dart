import 'package:flutter/material.dart';
import 'package:qyre_test/icons/job_offer.painter.dart';
import 'package:qyre_test/components/atoms/custom_title.dart';
import 'package:qyre_test/components/molecules/empty_body.dart';
import 'package:qyre_test/components/atoms/job_card.dart';
import 'package:qyre_test/domain/job_offer/entities/job_offer.entity.dart';

class JobOfferBody extends StatelessWidget {
  final List<JobOfferEntity> jobOffers;

  const JobOfferBody({this.jobOffers = const [], super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
      child: Column(
        children: [
          const CustomTitle(
            title: 'My job offers',
          ),
          if (jobOffers.isEmpty)
            const EmptyBody(
              painter: JobOfferPainter(),
              title: 'Job offers are shown here! Keep your profile updated to stay relevant for new opportunities.',
            ),
          for (int i = 0; i < jobOffers.length * 2 - 1; ++i)
            if (i.isOdd)
              const SizedBox(height: 10)
            else
              JobCard(
                title: jobOffers[i ~/ 2].title,
                initDate: jobOffers[i ~/ 2].initDate,
                subTitle: jobOffers[i ~/ 2].subTitle,
                startDate: jobOffers[i ~/ 2].startDate,
                endDate: jobOffers[i ~/ 2].endDate,
                duration: jobOffers[i ~/ 2].duration,
              )
        ],
      ),
    );
  }
}
