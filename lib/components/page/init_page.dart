import 'dart:developer';
import 'dart:ui';

import 'package:intl/intl.dart';
import 'package:qyre_test/components/atoms/align_positoned.dart';
import 'package:qyre_test/icons/qyre_home_line.painter.dart';
import 'package:qyre_test/icons/qyre_menu_line.painter.dart';
import 'package:qyre_test/icons/qyre_notification_line.painter.dart';
import 'package:qyre_test/components/molecules/active_body.dart';
import 'package:qyre_test/components/molecules/custom_app_bar.dart';
import 'package:qyre_test/components/atoms/day_card.dart';
import 'package:qyre_test/components/molecules/job_offer_body.dart';
import 'package:qyre_test/components/molecules/production_body.dart';
import 'package:qyre_test/components/molecules/starred_body.dart';
import 'dart:math' as math;

import 'package:qyre_test/components/atoms/suggest_card.dart';
import 'package:flutter/material.dart';
import 'package:qyre_test/domain/job_offer/entities/job_offer.entity.dart';
import 'package:qyre_test/domain/production/entities/production.entity.dart';
import 'package:qyre_test/domain/starred/entities/starred.entity.dart';
import 'package:qyre_test/utils/color.dart';

final testProduction = [
  ProductionEntity(
    country: 'Sweden',
    title: 'Production Name That is Long',
    startDateTime: DateTime(2022, 1, 14),
    endDateTime: DateTime(2023, 4, 23),
    imageUrl: 'https://pbs.twimg.com/profile_images/839881982574108672/qN7BVlr4_400x400.jpg',
  ),
  ProductionEntity(
    country: 'Sweden',
    title:
        "What has bee seen very very long tensity. Okey It is long long longe lnger super text. That's why i wrte long text. Why not, Why not yes. I can do it. I will do it)",
    startDateTime: DateTime(2022, 1, 14),
    endDateTime: DateTime(2023, 4, 23),
    imageUrl: 'https://pbs.twimg.com/profile_images/839881982574108672/qN7BVlr4_400x400.jpg',
  ),
];
final testJobOffest = [
  JobOfferEntity(
    title: 'Boom operator',
    initDate: DateTime.now(),
    subTitle: 'Masterchef',
    startDate: DateTime.now(),
    endDate: DateTime.now(),
    duration: const Duration(days: 5),
  ),
];
const starTest = [
  StarredEntity(
    title: 'Qyre US Production',
    subTitle: 'Updated priviligies for current',
    duration: Duration(days: 4),
    description:
        'I changed your admin roles to posters. With that you can’t send out offers. Just use the communication tool to get all the features!',
  ),
];

class InitPage extends StatefulWidget {
  const InitPage({super.key});

  @override
  State<InitPage> createState() => _InitPageState();
}

class _InitPageState extends State<InitPage> {
  final ScrollController scrollController = ScrollController();
  final initDate = DateTime.now();
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final random = math.Random();
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          ListView(
            key: const Key("main_scroll"),
            controller: scrollController,
            padding: EdgeInsets.only(top: mediaQuery.padding.top + 54),
            children: [
              SizedBox(
                height: 125,
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  padding: const EdgeInsets.only(top: 4, bottom: 10, left: 16, right: 16),
                  itemBuilder: (ctx, index) {
                    final date = initDate.add(Duration(days: index));
                    DayCardType? dayCardType;

                    final ind = random.nextInt(3);
                    if (ind == 1) {
                      dayCardType = DayCardType.red;
                    } else if (ind == 2) {
                      dayCardType = DayCardType.blue;
                    }
                    return DayCard(
                      subTitle: DateFormat("EEE").format(date),
                      label: DateFormat("MMM").format(date),
                      dayLabel: DateFormat("dd").format(date),
                      type: dayCardType,
                    );
                  },
                  separatorBuilder: (context, index) => const SizedBox(width: 8),
                  itemCount: 7,
                ),
              ),
              SizedBox(
                height: 156,
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                  itemBuilder: (ctx, i) {
                    return const SuggestCard(
                      title: 'Complete your profile tooptimize your exposure in job searches.',
                      label: 'Complete profile',
                      percentage: 0.4,
                    );
                  },
                  separatorBuilder: (ctx, i) => const SizedBox(width: 10),
                  itemCount: 3,
                ),
              ),
              ProductionBody(productions: testProduction),
              const ActiveBody(),
              JobOfferBody(jobOffers: testJobOffest),
              const StarredBody(starred: starTest),
              const SizedBox(height: 100),
            ],
          ),
          Positioned(
            left: 0,
            right: 0,
            top: 0,
            child: ClipRRect(
              child: BackdropFilter(
                filter: ImageFilter.blur(
                  sigmaX: 5,
                  sigmaY: 5,
                ),
                child: CustomAppBar(
                  title: 'My Availability',
                  scrollController: scrollController,
                ),
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        color: ColorData.n1Black100,
        padding: EdgeInsets.only(
          left: 30,
          right: 30,
          bottom: 6 + MediaQuery.of(context).padding.bottom,
          top: 6,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: () {
                log("DWAdw");
              },
              child: const Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                child: CustomPaint(
                  painter: QyreHomeLinePainter(color: Colors.white),
                  size: Size(24, 24),
                ),
              ),
            ),
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                log('message');
              },
              child: const Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                child: CustomPaint(
                  painter: QyreMenuLinePainter(color: Colors.white),
                  size: Size(24, 24),
                ),
              ),
            ),
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                log('message1');
              },
              child: Stack(
                children: [
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                    child: CustomPaint(
                      painter: QyreNotificationCustomPainter(color: Colors.white),
                      size: Size(24, 24),
                    ),
                  ),
                  Positioned.fill(
                    child: AlignPositioned(
                      centerPoint: const Offset(37, 12),
                      child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 2),
                        decoration: const ShapeDecoration(
                          shape: CircleBorder(),
                          color: ColorData.n18RedBright,
                        ),
                        child: const Text(
                          '10',
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 10,
                            height: 12 / 10,
                            color: ColorData.n10WhiteNumber100,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
