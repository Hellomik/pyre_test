// Need back to be more accurate

class ProductionEntity {
  final String country;
  final String title;
  final DateTime startDateTime;
  final DateTime endDateTime;
  final String imageUrl;

  const ProductionEntity({
    required this.country,
    required this.title,
    required this.startDateTime,
    required this.endDateTime,
    required this.imageUrl,
  });
}
