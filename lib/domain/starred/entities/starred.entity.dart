class StarredEntity {
  final String title;
  final String subTitle;
  final Duration duration;
  final String description;

  const StarredEntity({
    required this.title,
    required this.subTitle,
    required this.duration,
    required this.description,
  });
}
