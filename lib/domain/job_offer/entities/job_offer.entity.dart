class JobOfferEntity {
  final String title;
  final DateTime initDate;
  final String subTitle;
  final DateTime startDate;
  final DateTime endDate;
  final Duration duration;

  const JobOfferEntity({
    required this.title,
    required this.initDate,
    required this.subTitle,
    required this.startDate,
    required this.endDate,
    required this.duration,
  });
}
