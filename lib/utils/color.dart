import 'package:flutter/material.dart';

abstract class ColorData {
  static const Color n1Black100 = Color(0xff111111);
  static const Color n10WhiteNumber100 = Color(0xffFFFFFF);
  static const Color n5Black10 = Color(0xff656565);
  static const Color n9Gray25 = Color(0xffF0F2F5);
  static const Color n3Black50 = Color(0xff444444);
  static const Color n12BlueFaded = Color(0xff87C6F5);
  static const Color n18RedBright = Color(0xffEC4E27);
  static const Color n6Gray100 = Color(0xff9C9C9C);
  static LinearGradient get n20GradientBlueGradientColors => const LinearGradient(
        colors: [
          Color(0xff3465C3),
          Color(0xff5785DE),
        ],
      );
  static LinearGradient get n21GradientRedColors => const LinearGradient(
        colors: [
          Color(0xffEC4E27),
          Color(0xffF47E61),
        ],
      );
  static LinearGradient get n22GradientPurpleColors => const LinearGradient(
        colors: [
          Color(0xff6B34C3),
          Color(0xff8E5EDB),
        ],
      );
}
