import 'package:flutter/material.dart';

TextStyle getEffectiveTextStyle({
  required TextStyle style,
  required BuildContext context,
}) {
  final DefaultTextStyle defaultTextStyle = DefaultTextStyle.of(context);
  TextStyle effectiveTextStyle = style;
  if (style.inherit) {
    effectiveTextStyle = defaultTextStyle.style.merge(style);
  }
  if (MediaQuery.boldTextOverride(context)) {
    effectiveTextStyle = effectiveTextStyle.merge(const TextStyle(fontWeight: FontWeight.bold));
  }
  return effectiveTextStyle;
}
