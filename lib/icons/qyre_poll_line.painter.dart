import 'package:flutter/material.dart';

class QyrePollLinePainter extends CustomPainter {
  final Color color;

  const QyrePollLinePainter({this.color = Colors.black});

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.2500000, size.height * 0.08333333);
    path_0.lineTo(size.width * 0.08333333, size.height * 0.08333333);
    path_0.lineTo(size.width * 0.08333333, size.height * 0.2500000);
    path_0.lineTo(size.width * 0.2500000, size.height * 0.2500000);
    path_0.lineTo(size.width * 0.2500000, size.height * 0.08333333);
    path_0.close();

    Paint paint0Fill = Paint()..style = PaintingStyle.fill;
    paint0Fill.color = color;
    canvas.drawPath(path_0, paint0Fill);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.9166667, size.height * 0.1250000);
    path_1.lineTo(size.width * 0.3333333, size.height * 0.1250000);
    path_1.lineTo(size.width * 0.3333333, size.height * 0.2083333);
    path_1.lineTo(size.width * 0.9166667, size.height * 0.2083333);
    path_1.lineTo(size.width * 0.9166667, size.height * 0.1250000);
    path_1.close();

    Paint paint1Fill = Paint()..style = PaintingStyle.fill;
    paint1Fill.color = color;
    canvas.drawPath(path_1, paint1Fill);

    Path path_2 = Path();
    path_2.moveTo(size.width * 0.9166667, size.height * 0.7916667);
    path_2.lineTo(size.width * 0.3333333, size.height * 0.7916667);
    path_2.lineTo(size.width * 0.3333333, size.height * 0.8750000);
    path_2.lineTo(size.width * 0.9166667, size.height * 0.8750000);
    path_2.lineTo(size.width * 0.9166667, size.height * 0.7916667);
    path_2.close();

    Paint paint2Fill = Paint()..style = PaintingStyle.fill;
    paint2Fill.color = color;
    canvas.drawPath(path_2, paint2Fill);

    Path path_3 = Path();
    path_3.moveTo(size.width * 0.3333333, size.height * 0.4583333);
    path_3.lineTo(size.width * 0.9166667, size.height * 0.4583333);
    path_3.lineTo(size.width * 0.9166667, size.height * 0.5416667);
    path_3.lineTo(size.width * 0.3333333, size.height * 0.5416667);
    path_3.lineTo(size.width * 0.3333333, size.height * 0.4583333);
    path_3.close();

    Paint paint3Fill = Paint()..style = PaintingStyle.fill;
    paint3Fill.color = color;
    canvas.drawPath(path_3, paint3Fill);

    Path path_4 = Path();
    path_4.moveTo(size.width * 0.08333333, size.height * 0.4166667);
    path_4.lineTo(size.width * 0.2500000, size.height * 0.4166667);
    path_4.lineTo(size.width * 0.2500000, size.height * 0.5833333);
    path_4.lineTo(size.width * 0.08333333, size.height * 0.5833333);
    path_4.lineTo(size.width * 0.08333333, size.height * 0.4166667);
    path_4.close();

    Paint paint4Fill = Paint()..style = PaintingStyle.fill;
    paint4Fill.color = color;
    canvas.drawPath(path_4, paint4Fill);

    Path path_5 = Path();
    path_5.moveTo(size.width * 0.2500000, size.height * 0.7500000);
    path_5.lineTo(size.width * 0.08333333, size.height * 0.7500000);
    path_5.lineTo(size.width * 0.08333333, size.height * 0.9166667);
    path_5.lineTo(size.width * 0.2500000, size.height * 0.9166667);
    path_5.lineTo(size.width * 0.2500000, size.height * 0.7500000);
    path_5.close();

    Paint paint5Fill = Paint()..style = PaintingStyle.fill;
    paint5Fill.color = color;
    canvas.drawPath(path_5, paint5Fill);
  }

  @override
  bool shouldRepaint(covariant QyrePollLinePainter oldDelegate) {
    return oldDelegate.color != color;
  }
}
