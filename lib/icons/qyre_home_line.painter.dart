import 'package:flutter/material.dart';

class QyreHomeLinePainter extends CustomPainter {
  final Color color;

  const QyreHomeLinePainter({this.color = Colors.black, super.repaint});
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.3750000, size.height * 0.5833333);
    path_0.lineTo(size.width * 0.6250000, size.height * 0.5833333);
    path_0.lineTo(size.width * 0.6250000, size.height * 0.8333333);
    path_0.lineTo(size.width * 0.8333333, size.height * 0.8333333);
    path_0.lineTo(size.width * 0.8333333, size.height * 0.4567167);
    path_0.lineTo(size.width * 0.5000000, size.height * 0.1900521);
    path_0.lineTo(size.width * 0.1666667, size.height * 0.4567208);
    path_0.lineTo(size.width * 0.1666667, size.height * 0.8333333);
    path_0.lineTo(size.width * 0.3750000, size.height * 0.8333333);
    path_0.lineTo(size.width * 0.3750000, size.height * 0.5833333);
    path_0.close();
    path_0.moveTo(size.width * 0.08333333, size.height * 0.4166667);
    path_0.lineTo(size.width * 0.5000000, size.height * 0.08333333);
    path_0.lineTo(size.width * 0.9166667, size.height * 0.4166667);
    path_0.lineTo(size.width * 0.9166667, size.height * 0.9166667);
    path_0.lineTo(size.width * 0.5416667, size.height * 0.9166667);
    path_0.lineTo(size.width * 0.5416667, size.height * 0.6666667);
    path_0.lineTo(size.width * 0.4583333, size.height * 0.6666667);
    path_0.lineTo(size.width * 0.4583333, size.height * 0.9166667);
    path_0.lineTo(size.width * 0.08333333, size.height * 0.9166667);
    path_0.lineTo(size.width * 0.08333333, size.height * 0.4166667);
    path_0.close();

    Paint paint0Fill = Paint()..style = PaintingStyle.fill;
    paint0Fill.color = color;
    canvas.drawPath(path_0, paint0Fill);
  }

  @override
  bool shouldRepaint(covariant QyreHomeLinePainter oldDelegate) {
    return oldDelegate.color != color;
  }
}
