import 'package:flutter/material.dart';

class QyreAttachedPciturePainter extends CustomPainter {
  final Color color;

  const QyreAttachedPciturePainter({
    this.color = Colors.black,
    super.repaint,
  });

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.6666667, size.height * 0.4166667);
    path_0.cubicTo(size.width * 0.7126917, size.height * 0.4166667, size.width * 0.7500000, size.height * 0.3793571,
        size.width * 0.7500000, size.height * 0.3333333);
    path_0.cubicTo(size.width * 0.7500000, size.height * 0.2873096, size.width * 0.7126917, size.height * 0.2500000,
        size.width * 0.6666667, size.height * 0.2500000);
    path_0.cubicTo(size.width * 0.6206417, size.height * 0.2500000, size.width * 0.5833333, size.height * 0.2873096,
        size.width * 0.5833333, size.height * 0.3333333);
    path_0.cubicTo(size.width * 0.5833333, size.height * 0.3793571, size.width * 0.6206417, size.height * 0.4166667,
        size.width * 0.6666667, size.height * 0.4166667);
    path_0.close();

    Paint paint0Fill = Paint()..style = PaintingStyle.fill;
    paint0Fill.color = color;
    canvas.drawPath(path_0, paint0Fill);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.9166667, size.height * 0.08333333);
    path_1.lineTo(size.width * 0.08333333, size.height * 0.08333333);
    path_1.lineTo(size.width * 0.08333333, size.height * 0.9166667);
    path_1.lineTo(size.width * 0.9166667, size.height * 0.9166667);
    path_1.lineTo(size.width * 0.9166667, size.height * 0.08333333);
    path_1.close();
    path_1.moveTo(size.width * 0.1666667, size.height * 0.1666667);
    path_1.lineTo(size.width * 0.8333333, size.height * 0.1666667);
    path_1.lineTo(size.width * 0.8333333, size.height * 0.6312167);
    path_1.lineTo(size.width * 0.7117875, size.height * 0.4792875);
    path_1.lineTo(size.width * 0.5843417, size.height * 0.6067333);
    path_1.lineTo(size.width * 0.3793367, size.height * 0.3561679);
    path_1.lineTo(size.width * 0.1666667, size.height * 0.5511167);
    path_1.lineTo(size.width * 0.1666667, size.height * 0.1666667);
    path_1.close();
    path_1.moveTo(size.width * 0.7048792, size.height * 0.6040458);
    path_1.lineTo(size.width * 0.8333333, size.height * 0.7646167);
    path_1.lineTo(size.width * 0.8333333, size.height * 0.8333333);
    path_1.lineTo(size.width * 0.7697458, size.height * 0.8333333);
    path_1.lineTo(size.width * 0.6373750, size.height * 0.6715500);
    path_1.lineTo(size.width * 0.7048792, size.height * 0.6040458);
    path_1.close();
    path_1.moveTo(size.width * 0.1666667, size.height * 0.6641625);
    path_1.lineTo(size.width * 0.3706633, size.height * 0.4771667);
    path_1.lineTo(size.width * 0.6620750, size.height * 0.8333333);
    path_1.lineTo(size.width * 0.1666667, size.height * 0.8333333);
    path_1.lineTo(size.width * 0.1666667, size.height * 0.6641625);
    path_1.close();

    Paint paint1Fill = Paint()..style = PaintingStyle.fill;
    paint1Fill.color = color;
    canvas.drawPath(path_1, paint1Fill);
  }

  @override
  bool shouldRepaint(covariant QyreAttachedPciturePainter oldDelegate) {
    return oldDelegate.color != color;
  }
}
