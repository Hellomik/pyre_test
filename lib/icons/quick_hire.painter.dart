import 'package:flutter/material.dart';

//Copy this CustomPainter code to the Bottom of the File
class QuickHirePainter extends CustomPainter {
  final Color color;

  const QuickHirePainter({this.color = Colors.black, super.repaint});

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.3333344, 0);
    path_0.lineTo(size.width, 0);
    path_0.lineTo(size.width, size.height * 0.3333313);
    path_0.lineTo(size.width * 0.6666656, size.height * 0.6666250);
    path_0.lineTo(size.width * 0.6666656, size.height * 0.3333313);
    path_0.lineTo(size.width * 0.3333344, size.height * 0.3333313);
    path_0.lineTo(size.width * 0.3333344, size.height * 0.6666250);
    path_0.lineTo(size.width * 0.6666656, size.height * 0.6666250);
    path_0.lineTo(size.width * 0.6666219, size.height * 0.6666688);
    path_0.lineTo(size.width * 0.9952000, size.height * 0.6666688);
    path_0.lineTo(size.width * 0.9952000, size.height);
    path_0.lineTo(size.width * 0.6618656, size.height);
    path_0.lineTo(size.width * 0.6618656, size.height * 0.6714281);
    path_0.lineTo(size.width * 0.3333344, size.height * 0.9999562);
    path_0.lineTo(0, size.height * 0.9999562);
    path_0.lineTo(0, size.height * 0.3333313);
    path_0.lineTo(size.width * 0.3333344, 0);
    path_0.close();

    Paint paint0Fill = Paint()..style = PaintingStyle.fill;
    paint0Fill.color = color;

    path_0.fillType = PathFillType.evenOdd;
    canvas.drawPath(path_0, paint0Fill);
  }

  @override
  bool shouldRepaint(covariant QuickHirePainter oldDelegate) {
    return oldDelegate.color != color;
  }
}
