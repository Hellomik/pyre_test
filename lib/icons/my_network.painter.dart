import 'package:flutter/material.dart';

class MyNetworkPainter extends CustomPainter {
  final Color color;

  const MyNetworkPainter({this.color = Colors.black, super.repaint});

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(0, size.height * 0.5378147);
    path_0.lineTo(size.width * 0.6363640, size.height * 0.5378147);
    path_0.lineTo(size.width * 0.6363640, size.height);
    path_0.lineTo(0, size.height);
    path_0.lineTo(0, size.height * 0.5378147);
    path_0.close();
    path_0.moveTo(size.width * 0.7272720, size.height * 0.5378147);
    path_0.lineTo(size.width, size.height * 0.5378147);
    path_0.lineTo(size.width, size.height);
    path_0.lineTo(size.width * 0.7272720, size.height);
    path_0.lineTo(size.width * 0.7272720, size.height * 0.5378147);
    path_0.close();
    path_0.moveTo(size.width * 0.6818180, size.height * 0.4033618);
    path_0.cubicTo(size.width * 0.7572720, size.height * 0.4033618, size.width * 0.8177280, size.height * 0.3132765,
        size.width * 0.8177280, size.height * 0.2016806);
    path_0.cubicTo(
        size.width * 0.8177280, size.height * 0.09008412, size.width * 0.7572720, 0, size.width * 0.6818180, 0);
    path_0.cubicTo(size.width * 0.6063640, 0, size.width * 0.5454540, size.height * 0.09008412, size.width * 0.5454540,
        size.height * 0.2016806);
    path_0.cubicTo(size.width * 0.5454540, size.height * 0.3132765, size.width * 0.6063640, size.height * 0.4033618,
        size.width * 0.6818180, size.height * 0.4033618);
    path_0.close();
    path_0.moveTo(size.width * 0.3181820, size.height * 0.4033618);
    path_0.cubicTo(size.width * 0.3936360, size.height * 0.4033618, size.width * 0.4540900, size.height * 0.3132765,
        size.width * 0.4540900, size.height * 0.2016806);
    path_0.cubicTo(
        size.width * 0.4540900, size.height * 0.09008412, size.width * 0.3936360, 0, size.width * 0.3181820, 0);
    path_0.cubicTo(size.width * 0.2427280, 0, size.width * 0.1818182, size.height * 0.09008412, size.width * 0.1818182,
        size.height * 0.2016806);
    path_0.cubicTo(size.width * 0.1818182, size.height * 0.3132765, size.width * 0.2427280, size.height * 0.4033618,
        size.width * 0.3181820, size.height * 0.4033618);
    path_0.close();

    Paint paint0Fill = Paint()..style = PaintingStyle.fill;
    paint0Fill.color = color;
    canvas.drawPath(path_0, paint0Fill);
  }

  @override
  bool shouldRepaint(covariant MyNetworkPainter oldDelegate) {
    return oldDelegate.color != color;
  }
}
