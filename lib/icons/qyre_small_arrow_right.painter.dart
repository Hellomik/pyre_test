import 'package:flutter/material.dart';

class QyreSmallArrowRightPainter extends CustomPainter {
  final Color color;

  const QyreSmallArrowRightPainter({this.color = Colors.black, super.repaint});

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.5585800, size.height * 0.5000000);
    path_0.lineTo(size.width * 0.2792910, size.height * 0.2461009);
    path_0.lineTo(size.width * 0.4207120, size.height * 0.1175355);
    path_0.lineTo(size.width * 0.8414230, size.height * 0.5000000);
    path_0.lineTo(size.width * 0.4207120, size.height * 0.8824645);
    path_0.lineTo(size.width * 0.2792910, size.height * 0.7538991);
    path_0.lineTo(size.width * 0.5585800, size.height * 0.5000000);
    path_0.close();

    Paint paint0Fill = Paint()..style = PaintingStyle.fill;
    paint0Fill.color = color;
    canvas.drawPath(path_0, paint0Fill);
  }

  @override
  bool shouldRepaint(covariant QyreSmallArrowRightPainter oldDelegate) {
    return oldDelegate.color != color;
  }
}
