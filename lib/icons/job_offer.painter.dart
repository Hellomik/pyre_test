import 'package:flutter/material.dart';

class JobOfferPainter extends CustomPainter {
  const JobOfferPainter({super.repaint});

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint0Fill = Paint()..style = PaintingStyle.fill;
    paint0Fill.color = const Color(0xffCDCECE).withOpacity(1.0);
    canvas.drawRRect(
        RRect.fromRectAndCorners(
            Rect.fromLTWH(
                size.width * 0.03703704, size.height * 0.6851852, size.width * 0.9259259, size.height * 0.2407407),
            bottomRight: Radius.circular(size.width * 0.05555556),
            bottomLeft: Radius.circular(size.width * 0.05555556),
            topLeft: Radius.circular(size.width * 0.05555556),
            topRight: Radius.circular(size.width * 0.05555556)),
        paint0Fill);

    Paint paint1Fill = Paint()..style = PaintingStyle.fill;
    paint1Fill.color = const Color(0xffE6E9EA).withOpacity(1.0);
    canvas.drawRect(
        Rect.fromLTWH(
            size.width * 0.3888889, size.height * 0.7407407, size.width * 0.4259259, size.height * 0.05555556),
        paint1Fill);

    Paint paint2Fill = Paint()..style = PaintingStyle.fill;
    paint2Fill.color = const Color(0xffE6E9EA).withOpacity(1.0);
    canvas.drawRect(
        Rect.fromLTWH(
            size.width * 0.3888889, size.height * 0.8148148, size.width * 0.2592593, size.height * 0.05555556),
        paint2Fill);

    Paint paint3Fill = Paint()..style = PaintingStyle.fill;
    paint3Fill.color = const Color(0xffE6E9EA).withOpacity(1.0);
    canvas.drawCircle(Offset(size.width * 0.2129630, size.height * 0.8055556), size.width * 0.08333333, paint3Fill);

    Paint paint4Fill = Paint()..style = PaintingStyle.fill;
    paint4Fill.color = const Color(0xffA0A2A3).withOpacity(1.0);
    canvas.drawRRect(
        RRect.fromRectAndCorners(
            Rect.fromLTWH(
                size.width * 0.03703704, size.height * 0.3703704, size.width * 0.9259259, size.height * 0.2592593),
            bottomRight: Radius.circular(size.width * 0.05555556),
            bottomLeft: Radius.circular(size.width * 0.05555556),
            topLeft: Radius.circular(size.width * 0.05555556),
            topRight: Radius.circular(size.width * 0.05555556)),
        paint4Fill);

    Paint paint5Fill = Paint()..style = PaintingStyle.fill;
    paint5Fill.color = const Color(0xffE6E9EA).withOpacity(1.0);
    canvas.drawRect(
        Rect.fromLTWH(
            size.width * 0.3888889, size.height * 0.4259259, size.width * 0.4444444, size.height * 0.05555556),
        paint5Fill);

    Paint paint6Fill = Paint()..style = PaintingStyle.fill;
    paint6Fill.color = const Color(0xffE6E9EA).withOpacity(1.0);
    canvas.drawRect(
        Rect.fromLTWH(
            size.width * 0.3888889, size.height * 0.5185185, size.width * 0.2592593, size.height * 0.05555556),
        paint6Fill);

    Path path_7 = Path();
    path_7.moveTo(size.width * 0.3091741, size.height * 0.4498278);
    path_7.lineTo(size.width * 0.1901019, size.height * 0.5576870);
    path_7.lineTo(size.width * 0.1186026, size.height * 0.5046944);
    path_7.lineTo(size.width * 0.1406559, size.height * 0.4749389);
    path_7.lineTo(size.width * 0.1877519, size.height * 0.5098444);
    path_7.lineTo(size.width * 0.2843093, size.height * 0.4223778);
    path_7.lineTo(size.width * 0.3091741, size.height * 0.4498278);
    path_7.close();

    Paint paint7Fill = Paint()..style = PaintingStyle.fill;
    paint7Fill.color = Colors.white.withOpacity(1.0);
    path_7.fillType = PathFillType.evenOdd;

    canvas.drawPath(path_7, paint7Fill);

    Paint paint8Fill = Paint()..style = PaintingStyle.fill;
    paint8Fill.color = const Color(0xffA0A2A3).withOpacity(1.0);
    canvas.drawRRect(
        RRect.fromRectAndCorners(
            Rect.fromLTWH(
                size.width * 0.03703704, size.height * 0.05555556, size.width * 0.9259259, size.height * 0.2592593),
            bottomRight: Radius.circular(size.width * 0.07407407),
            bottomLeft: Radius.circular(size.width * 0.07407407),
            topLeft: Radius.circular(size.width * 0.07407407),
            topRight: Radius.circular(size.width * 0.07407407)),
        paint8Fill);

    Path path_9 = Path();
    path_9.moveTo(size.width * 0.3072870, size.height * 0.1436785);
    path_9.lineTo(size.width * 0.1895278, size.height * 0.2448093);
    path_9.lineTo(size.width * 0.1189835, size.height * 0.1952426);
    path_9.lineTo(size.width * 0.1402767, size.height * 0.1649387);
    path_9.lineTo(size.width * 0.1872500, size.height * 0.1979444);
    path_9.lineTo(size.width * 0.2831574, size.height * 0.1155807);
    path_9.lineTo(size.width * 0.3072870, size.height * 0.1436785);
    path_9.close();

    Paint paint9Fill = Paint()..style = PaintingStyle.fill;
    paint9Fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_9, paint9Fill);

    Paint paint10Fill = Paint()..style = PaintingStyle.fill;
    paint10Fill.color = const Color(0xffE6E9EA).withOpacity(1.0);
    canvas.drawRect(
        Rect.fromLTWH(
            size.width * 0.3888889, size.height * 0.1111111, size.width * 0.4444444, size.height * 0.05555556),
        paint10Fill);

    Paint paint11Fill = Paint()..style = PaintingStyle.fill;
    paint11Fill.color = const Color(0xffE6E9EA).withOpacity(1.0);
    canvas.drawRect(
        Rect.fromLTWH(
            size.width * 0.3888889, size.height * 0.2037037, size.width * 0.2592593, size.height * 0.05555556),
        paint11Fill);
  }

  @override
  bool shouldRepaint(covariant JobOfferPainter oldDelegate) {
    return false;
  }
}
