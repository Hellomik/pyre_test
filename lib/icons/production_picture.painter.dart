import 'package:flutter/material.dart';

class ProductionPainterPainter extends CustomPainter {
  final Color firstColor;
  final Color secondColor;

  const ProductionPainterPainter({
    this.firstColor = const Color(0xff9C9C9C),
    this.secondColor = const Color(0xffCDCECE),
    super.repaint,
  });

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.4778796, size.height * 0.1905278);
    path_0.lineTo(size.width * 0.1998111, size.height * 0.1382548);
    path_0.lineTo(size.width * 0.04629630, size.height * 0.7964315);
    path_0.lineTo(size.width * 0.3243648, size.height * 0.8487056);
    path_0.lineTo(size.width * 0.4778796, size.height * 0.1905278);
    path_0.close();
    path_0.moveTo(size.width * 0.3989704, size.height * 0.2676593);
    path_0.lineTo(size.width * 0.4204259, size.height * 0.2716926);
    path_0.lineTo(size.width * 0.4309019, size.height * 0.2267778);
    path_0.lineTo(size.width * 0.4094463, size.height * 0.2227444);
    path_0.lineTo(size.width * 0.3989704, size.height * 0.2676593);
    path_0.close();
    path_0.moveTo(size.width * 0.3784204, size.height * 0.3557630);
    path_0.lineTo(size.width * 0.3998778, size.height * 0.3597963);
    path_0.lineTo(size.width * 0.4103537, size.height * 0.3148815);
    path_0.lineTo(size.width * 0.3888981, size.height * 0.3108481);
    path_0.lineTo(size.width * 0.3784204, size.height * 0.3557630);
    path_0.close();
    path_0.moveTo(size.width * 0.3582741, size.height * 0.4421370);
    path_0.lineTo(size.width * 0.3797315, size.height * 0.4461704);
    path_0.lineTo(size.width * 0.3898037, size.height * 0.4029833);
    path_0.lineTo(size.width * 0.3683481, size.height * 0.3989500);
    path_0.lineTo(size.width * 0.3582741, size.height * 0.4421370);
    path_0.close();
    path_0.moveTo(size.width * 0.3377259, size.height * 0.5302389);
    path_0.lineTo(size.width * 0.3591815, size.height * 0.5342722);
    path_0.lineTo(size.width * 0.3692556, size.height * 0.4910852);
    path_0.lineTo(size.width * 0.3477981, size.height * 0.4870519);
    path_0.lineTo(size.width * 0.3377259, size.height * 0.5302389);
    path_0.close();
    path_0.moveTo(size.width * 0.3171759, size.height * 0.6183426);
    path_0.lineTo(size.width * 0.3386315, size.height * 0.6223759);
    path_0.lineTo(size.width * 0.3487056, size.height * 0.5791889);
    path_0.lineTo(size.width * 0.3272500, size.height * 0.5751537);
    path_0.lineTo(size.width * 0.3171759, size.height * 0.6183426);
    path_0.close();
    path_0.moveTo(size.width * 0.2966278, size.height * 0.7064444);
    path_0.lineTo(size.width * 0.3180833, size.height * 0.7104778);
    path_0.lineTo(size.width * 0.3285593, size.height * 0.6655630);
    path_0.lineTo(size.width * 0.3071037, size.height * 0.6615296);
    path_0.lineTo(size.width * 0.2966278, size.height * 0.7064444);
    path_0.close();
    path_0.moveTo(size.width * 0.2760778, size.height * 0.7945463);
    path_0.lineTo(size.width * 0.2975333, size.height * 0.7985796);
    path_0.lineTo(size.width * 0.3080093, size.height * 0.7536648);
    path_0.lineTo(size.width * 0.2865537, size.height * 0.7496315);
    path_0.lineTo(size.width * 0.2760778, size.height * 0.7945463);
    path_0.close();
    path_0.moveTo(size.width * 0.2161667, size.height * 0.2332944);
    path_0.lineTo(size.width * 0.2376222, size.height * 0.2373278);
    path_0.lineTo(size.width * 0.2480981, size.height * 0.1924130);
    path_0.lineTo(size.width * 0.2266426, size.height * 0.1883796);
    path_0.lineTo(size.width * 0.2161667, size.height * 0.2332944);
    path_0.close();
    path_0.moveTo(size.width * 0.1956167, size.height * 0.3213963);
    path_0.lineTo(size.width * 0.2170722, size.height * 0.3254315);
    path_0.lineTo(size.width * 0.2275500, size.height * 0.2805148);
    path_0.lineTo(size.width * 0.2060926, size.height * 0.2764815);
    path_0.lineTo(size.width * 0.1956167, size.height * 0.3213963);
    path_0.close();
    path_0.moveTo(size.width * 0.1754709, size.height * 0.4077722);
    path_0.lineTo(size.width * 0.1969259, size.height * 0.4118056);
    path_0.lineTo(size.width * 0.2070000, size.height * 0.3686185);
    path_0.lineTo(size.width * 0.1855444, size.height * 0.3645852);
    path_0.lineTo(size.width * 0.1754709, size.height * 0.4077722);
    path_0.close();
    path_0.moveTo(size.width * 0.1549215, size.height * 0.4958741);
    path_0.lineTo(size.width * 0.1763774, size.height * 0.4999074);
    path_0.lineTo(size.width * 0.1864500, size.height * 0.4567204);
    path_0.lineTo(size.width * 0.1649946, size.height * 0.4526870);
    path_0.lineTo(size.width * 0.1549215, size.height * 0.4958741);
    path_0.close();
    path_0.moveTo(size.width * 0.1343722, size.height * 0.5839759);
    path_0.lineTo(size.width * 0.1558281, size.height * 0.5880111);
    path_0.lineTo(size.width * 0.1659013, size.height * 0.5448222);
    path_0.lineTo(size.width * 0.1444454, size.height * 0.5407889);
    path_0.lineTo(size.width * 0.1343722, size.height * 0.5839759);
    path_0.close();
    path_0.moveTo(size.width * 0.1138230, size.height * 0.6720796);
    path_0.lineTo(size.width * 0.1352787, size.height * 0.6761130);
    path_0.lineTo(size.width * 0.1457548, size.height * 0.6311981);
    path_0.lineTo(size.width * 0.1242991, size.height * 0.6271648);
    path_0.lineTo(size.width * 0.1138230, size.height * 0.6720796);
    path_0.close();
    path_0.moveTo(size.width * 0.09327352, size.height * 0.7601815);
    path_0.lineTo(size.width * 0.1147294, size.height * 0.7642148);
    path_0.lineTo(size.width * 0.1252056, size.height * 0.7193000);
    path_0.lineTo(size.width * 0.1037496, size.height * 0.7152667);
    path_0.lineTo(size.width * 0.09327352, size.height * 0.7601815);
    path_0.close();

    Paint paint0Fill = Paint()..style = PaintingStyle.fill;
    paint0Fill.color = firstColor;
    path_0.fillType = PathFillType.evenOdd;
    canvas.drawPath(path_0, paint0Fill);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.4492074, size.height * 0.9052593);
    path_1.lineTo(size.width * 0.9409037, size.height * 0.7570963);
    path_1.lineTo(size.width * 0.8687889, size.height * 0.5164537);
    path_1.lineTo(size.width * 0.8757259, size.height * 0.5143685);
    path_1.lineTo(size.width * 0.8633093, size.height * 0.4729500);
    path_1.lineTo(size.width * 0.8808611, size.height * 0.4301259);
    path_1.lineTo(size.width * 0.8462556, size.height * 0.4226741);
    path_1.lineTo(size.width * 0.8006648, size.height * 0.4758981);
    path_1.lineTo(size.width * 0.8400019, size.height * 0.5251296);
    path_1.lineTo(size.width * 0.8091167, size.height * 0.5344407);
    path_1.lineTo(size.width * 0.7697722, size.height * 0.4852093);
    path_1.lineTo(size.width * 0.7054852, size.height * 0.5045833);
    path_1.lineTo(size.width * 0.7448241, size.height * 0.5538056);
    path_1.lineTo(size.width * 0.7139407, size.height * 0.5631185);
    path_1.lineTo(size.width * 0.6746037, size.height * 0.5138889);
    path_1.lineTo(size.width * 0.6103093, size.height * 0.5332611);
    path_1.lineTo(size.width * 0.6496537, size.height * 0.5824926);
    path_1.lineTo(size.width * 0.6187630, size.height * 0.5917963);
    path_1.lineTo(size.width * 0.5794259, size.height * 0.5425667);
    path_1.lineTo(size.width * 0.5151389, size.height * 0.5619407);
    path_1.lineTo(size.width * 0.5544759, size.height * 0.6111704);
    path_1.lineTo(size.width * 0.5235852, size.height * 0.6204741);
    path_1.lineTo(size.width * 0.4842463, size.height * 0.5712500);
    path_1.lineTo(size.width * 0.4199593, size.height * 0.5906241);
    path_1.lineTo(size.width * 0.4593000, size.height * 0.6398463);
    path_1.lineTo(size.width * 0.4284148, size.height * 0.6491593);
    path_1.lineTo(size.width * 0.3890704, size.height * 0.5999278);
    path_1.lineTo(size.width * 0.3533611, size.height * 0.6106907);
    path_1.lineTo(size.width * 0.3701500, size.height * 0.6667167);
    path_1.lineTo(size.width * 0.3770889, size.height * 0.6646241);
    path_1.lineTo(size.width * 0.4492074, size.height * 0.9052593);
    path_1.close();
    path_1.moveTo(size.width * 0.8163278, size.height * 0.4162352);
    path_1.lineTo(size.width * 0.7540333, size.height * 0.4028204);
    path_1.lineTo(size.width * 0.7084519, size.height * 0.4560481);
    path_1.lineTo(size.width * 0.7707463, size.height * 0.4694611);
    path_1.lineTo(size.width * 0.8163278, size.height * 0.4162352);
    path_1.close();
    path_1.moveTo(size.width * 0.7241093, size.height * 0.3963741);
    path_1.lineTo(size.width * 0.6618222, size.height * 0.3829630);
    path_1.lineTo(size.width * 0.6162389, size.height * 0.4361963);
    path_1.lineTo(size.width * 0.6785278, size.height * 0.4496019);
    path_1.lineTo(size.width * 0.7241093, size.height * 0.3963741);
    path_1.close();
    path_1.moveTo(size.width * 0.6318944, size.height * 0.3765241);
    path_1.lineTo(size.width * 0.5696000, size.height * 0.3631111);
    path_1.lineTo(size.width * 0.5240185, size.height * 0.4163370);
    path_1.lineTo(size.width * 0.5863056, size.height * 0.4297500);
    path_1.lineTo(size.width * 0.6318944, size.height * 0.3765241);
    path_1.close();
    path_1.moveTo(size.width * 0.5396759, size.height * 0.3566648);
    path_1.lineTo(size.width * 0.4773815, size.height * 0.3432500);
    path_1.lineTo(size.width * 0.4317981, size.height * 0.3964852);
    path_1.lineTo(size.width * 0.4940926, size.height * 0.4098981);
    path_1.lineTo(size.width * 0.5396759, size.height * 0.3566648);
    path_1.close();
    path_1.moveTo(size.width * 0.4474537, size.height * 0.3368111);
    path_1.lineTo(size.width * 0.3910019, size.height * 0.3246574);
    path_1.lineTo(size.width * 0.3672667, size.height * 0.3825870);
    path_1.lineTo(size.width * 0.4018722, size.height * 0.3900389);
    path_1.lineTo(size.width * 0.4474537, size.height * 0.3368111);
    path_1.close();
    path_1.moveTo(size.width * 0.7035722, size.height * 0.6469000);
    path_1.lineTo(size.width * 0.4428019, size.height * 0.7254796);
    path_1.lineTo(size.width * 0.4500889, size.height * 0.7497889);
    path_1.lineTo(size.width * 0.7108593, size.height * 0.6712111);
    path_1.lineTo(size.width * 0.7035722, size.height * 0.6469000);
    path_1.close();
    path_1.moveTo(size.width * 0.7188407, size.height * 0.6978315);
    path_1.lineTo(size.width * 0.5334870, size.height * 0.7536833);
    path_1.lineTo(size.width * 0.5407648, size.height * 0.7779926);
    path_1.lineTo(size.width * 0.7261278, size.height * 0.7221426);
    path_1.lineTo(size.width * 0.7188407, size.height * 0.6978315);
    path_1.close();

    Paint paint1Fill = Paint()..style = PaintingStyle.fill;
    paint1Fill.color = secondColor;
    path_1.fillType = PathFillType.evenOdd;
    canvas.drawPath(path_1, paint1Fill);

    Path path_2 = Path();
    path_2.moveTo(size.width * 0.8658352, size.height * 0.2118074);
    path_2.lineTo(size.width * 0.6001074, size.height * 0.3495407);
    path_2.lineTo(size.width * 0.6001074, size.height * 0.07407407);
    path_2.lineTo(size.width * 0.8658352, size.height * 0.2118074);
    path_2.close();

    Paint paint2Fill = Paint()..style = PaintingStyle.fill;
    paint2Fill.color = secondColor;
    canvas.drawPath(path_2, paint2Fill);
  }

  @override
  bool shouldRepaint(covariant ProductionPainterPainter oldDelegate) {
    return oldDelegate.firstColor != firstColor || oldDelegate.secondColor != secondColor;
  }
}
