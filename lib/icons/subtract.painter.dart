import 'package:flutter/material.dart';

//Copy this CustomPainter code to the Bottom of the File
class SubtractPainter extends CustomPainter {
  final Color color;

  const SubtractPainter({this.color = Colors.black, super.repaint});

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width, 0);
    path_0.lineTo(size.width * 0.3333344, 0);
    path_0.lineTo(0, size.height * 0.3333313);
    path_0.lineTo(0, size.height * 0.9999562);
    path_0.lineTo(size.width, size.height * 0.9999562);
    path_0.lineTo(size.width, 0);
    path_0.close();
    path_0.moveTo(size.width * 0.1875000, size.height * 0.4062500);
    path_0.lineTo(size.width * 0.8125000, size.height * 0.4062500);
    path_0.lineTo(size.width * 0.8125000, size.height * 0.5000000);
    path_0.lineTo(size.width * 0.1875000, size.height * 0.5000000);
    path_0.lineTo(size.width * 0.1875000, size.height * 0.4062500);
    path_0.close();
    path_0.moveTo(size.width * 0.8125000, size.height * 0.6250000);
    path_0.lineTo(size.width * 0.1875000, size.height * 0.6250000);
    path_0.lineTo(size.width * 0.1875000, size.height * 0.7187500);
    path_0.lineTo(size.width * 0.8125000, size.height * 0.7187500);
    path_0.lineTo(size.width * 0.8125000, size.height * 0.6250000);
    path_0.close();

    Paint paint0Fill = Paint()..style = PaintingStyle.fill;
    paint0Fill.color = color;
    path_0.fillType = PathFillType.evenOdd;
    canvas.drawPath(path_0, paint0Fill);
  }

  @override
  bool shouldRepaint(covariant SubtractPainter oldDelegate) {
    return oldDelegate.color != color;
  }
}
