import 'package:flutter/material.dart';

class QyreMenuLinePainter extends CustomPainter {
  final Color color;

  const QyreMenuLinePainter({this.color = Colors.black, super.repaint});

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.9166667, size.height * 0.2083333);
    path_0.lineTo(size.width * 0.08333333, size.height * 0.2083333);
    path_0.lineTo(size.width * 0.08333333, size.height * 0.1250000);
    path_0.lineTo(size.width * 0.9166667, size.height * 0.1250000);
    path_0.lineTo(size.width * 0.9166667, size.height * 0.2083333);
    path_0.close();

    Paint paint0Fill = Paint()..style = PaintingStyle.fill;
    paint0Fill.color = color;
    canvas.drawPath(path_0, paint0Fill);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.9166667, size.height * 0.5416667);
    path_1.lineTo(size.width * 0.08333333, size.height * 0.5416667);
    path_1.lineTo(size.width * 0.08333333, size.height * 0.4583333);
    path_1.lineTo(size.width * 0.9166667, size.height * 0.4583333);
    path_1.lineTo(size.width * 0.9166667, size.height * 0.5416667);
    path_1.close();

    Paint paint1Fill = Paint()..style = PaintingStyle.fill;
    paint1Fill.color = color;
    canvas.drawPath(path_1, paint1Fill);

    Path path_2 = Path();
    path_2.moveTo(size.width * 0.08333333, size.height * 0.8750000);
    path_2.lineTo(size.width * 0.9166667, size.height * 0.8750000);
    path_2.lineTo(size.width * 0.9166667, size.height * 0.7916667);
    path_2.lineTo(size.width * 0.08333333, size.height * 0.7916667);
    path_2.lineTo(size.width * 0.08333333, size.height * 0.8750000);
    path_2.close();

    Paint paint2Fill = Paint()..style = PaintingStyle.fill;
    paint2Fill.color = color;
    canvas.drawPath(path_2, paint2Fill);
  }

  @override
  bool shouldRepaint(covariant QyreMenuLinePainter oldDelegate) {
    return oldDelegate.color != color;
  }
}
