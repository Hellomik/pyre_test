import 'package:flutter/material.dart';

class QyreNotificationCustomPainter extends CustomPainter {
  final Color color;

  const QyreNotificationCustomPainter({
    this.color = Colors.black,
    super.repaint,
  });

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.8487833, size.height * 0.2101400);
    path_0.lineTo(size.width * 0.9461292, size.height * 0.1127942);
    path_0.lineTo(size.width * 0.8872042, size.height * 0.05386875);
    path_0.lineTo(size.width * 0.7898583, size.height * 0.1512146);
    path_0.cubicTo(size.width * 0.6579167, size.height * 0.04829792, size.width * 0.4668917, size.height * 0.05751750,
        size.width * 0.3455375, size.height * 0.1788717);
    path_0.lineTo(size.width * 0.1250021, size.height * 0.3994071);
    path_0.lineTo(size.width * 0.07113000, size.height * 0.3455354);
    path_0.lineTo(size.width * 0.01220450, size.height * 0.4044608);
    path_0.lineTo(size.width * 0.1910758, size.height * 0.5833333);
    path_0.lineTo(size.width * 0.1788704, size.height * 0.5955375);
    path_0.cubicTo(size.width * 0.1165750, size.height * 0.6578333, size.width * 0.1165750, size.height * 0.7588333,
        size.width * 0.1788704, size.height * 0.8211292);
    path_0.cubicTo(size.width * 0.2411662, size.height * 0.8834250, size.width * 0.3421671, size.height * 0.8834250,
        size.width * 0.4044629, size.height * 0.8211292);
    path_0.lineTo(size.width * 0.4166667, size.height * 0.8089250);
    path_0.lineTo(size.width * 0.5955375, size.height * 0.9877958);
    path_0.lineTo(size.width * 0.6544625, size.height * 0.9288667);
    path_0.lineTo(size.width * 0.6005958, size.height * 0.8750000);
    path_0.lineTo(size.width * 0.8211292, size.height * 0.6544625);
    path_0.cubicTo(size.width * 0.9424833, size.height * 0.5331083, size.width * 0.9517042, size.height * 0.3420800,
        size.width * 0.8487833, size.height * 0.2101400);
    path_0.close();
    path_0.moveTo(size.width * 0.2377963, size.height * 0.6544625);
    path_0.lineTo(size.width * 0.2500013, size.height * 0.6422583);
    path_0.lineTo(size.width * 0.3577425, size.height * 0.7500000);
    path_0.lineTo(size.width * 0.3455371, size.height * 0.7622042);
    path_0.cubicTo(size.width * 0.3157854, size.height * 0.7919542, size.width * 0.2675479, size.height * 0.7919542,
        size.width * 0.2377963, size.height * 0.7622042);
    path_0.cubicTo(size.width * 0.2080442, size.height * 0.7324500, size.width * 0.2080442, size.height * 0.6842167,
        size.width * 0.2377963, size.height * 0.6544625);
    path_0.close();
    path_0.moveTo(size.width * 0.4044629, size.height * 0.2377971);
    path_0.cubicTo(size.width * 0.5032500, size.height * 0.1390096, size.width * 0.6634167, size.height * 0.1390096,
        size.width * 0.7622042, size.height * 0.2377971);
    path_0.cubicTo(size.width * 0.8609917, size.height * 0.3365846, size.width * 0.8609917, size.height * 0.4967500,
        size.width * 0.7622042, size.height * 0.5955375);
    path_0.lineTo(size.width * 0.5416667, size.height * 0.8160750);
    path_0.lineTo(size.width * 0.1839275, size.height * 0.4583333);
    path_0.lineTo(size.width * 0.4044629, size.height * 0.2377971);
    path_0.close();

    Paint paint0Fill = Paint()..style = PaintingStyle.fill;
    paint0Fill.color = color;
    canvas.drawPath(path_0, paint0Fill);
  }

  @override
  bool shouldRepaint(covariant QyreNotificationCustomPainter oldDelegate) {
    return oldDelegate.color != color;
  }
}
