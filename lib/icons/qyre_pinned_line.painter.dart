import 'package:flutter/material.dart';

class QyrePinnedLinePainter extends CustomPainter {
  final Color color;

  const QyrePinnedLinePainter({this.color = Colors.black, super.repaint});

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.6258667, size.height * 0.7437167);
    path_0.lineTo(size.width * 0.5669417, size.height * 0.6847917);
    path_0.lineTo(size.width * 0.8026458, size.height * 0.4490917);
    path_0.lineTo(size.width * 0.8321083, size.height * 0.4785542);
    path_0.lineTo(size.width * 0.8910333, size.height * 0.4196292);
    path_0.lineTo(size.width * 0.5964042, size.height * 0.1250000);
    path_0.lineTo(size.width * 0.5374792, size.height * 0.1839254);
    path_0.lineTo(size.width * 0.5669417, size.height * 0.2133883);
    path_0.lineTo(size.width * 0.3312396, size.height * 0.4490917);
    path_0.lineTo(size.width * 0.2723137, size.height * 0.3901650);
    path_0.lineTo(size.width * 0.2133883, size.height * 0.4490917);
    path_0.lineTo(size.width * 0.3607021, size.height * 0.5964042);
    path_0.lineTo(size.width * 0.1250000, size.height * 0.8321083);
    path_0.lineTo(size.width * 0.1839254, size.height * 0.8910333);
    path_0.lineTo(size.width * 0.4196292, size.height * 0.6553292);
    path_0.lineTo(size.width * 0.5669417, size.height * 0.8026458);
    path_0.lineTo(size.width * 0.6258667, size.height * 0.7437167);
    path_0.close();
    path_0.moveTo(size.width * 0.7437167, size.height * 0.3901650);
    path_0.lineTo(size.width * 0.5080167, size.height * 0.6258667);
    path_0.lineTo(size.width * 0.3901650, size.height * 0.5080167);
    path_0.lineTo(size.width * 0.6258667, size.height * 0.2723137);
    path_0.lineTo(size.width * 0.7437167, size.height * 0.3901650);
    path_0.close();

    Paint paint0Fill = Paint()..style = PaintingStyle.fill;
    paint0Fill.color = color;
    canvas.drawPath(path_0, paint0Fill);
  }

  @override
  bool shouldRepaint(covariant QyrePinnedLinePainter oldDelegate) {
    return oldDelegate.color != color;
  }
}
