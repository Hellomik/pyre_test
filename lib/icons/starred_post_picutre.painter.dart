import 'package:flutter/material.dart';

class StarredPostPicutrePainter extends CustomPainter {
  const StarredPostPicutrePainter({super.repaint});

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.3964056, size.height * 0.1464835);
    path_0.lineTo(size.width * 0.3327537, size.height * 0.1184642);
    path_0.lineTo(size.width * 0.3196537, size.height * 0.05233491);
    path_0.lineTo(size.width * 0.2731315, size.height * 0.1015409);
    path_0.lineTo(size.width * 0.2041500, size.height * 0.09191418);
    path_0.lineTo(size.width * 0.2391852, size.height * 0.1501795);
    path_0.lineTo(size.width * 0.2095037, size.height * 0.2101945);
    path_0.lineTo(size.width * 0.2776704, size.height * 0.1972145);
    path_0.lineTo(size.width * 0.3285222, size.height * 0.2439764);
    path_0.lineTo(size.width * 0.3354926, size.height * 0.1775153);
    path_0.lineTo(size.width * 0.3964056, size.height * 0.1464835);
    path_0.close();

    Paint paint0Fill = Paint()..style = PaintingStyle.fill;
    paint0Fill.color = const Color(0xffCDCECE).withOpacity(1.0);
    path_0.fillType = PathFillType.evenOdd;
    canvas.drawPath(path_0, paint0Fill);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.8446296, size.height * 0.5873836);
    path_1.lineTo(size.width * 0.8563630, size.height * 0.6219745);
    path_1.lineTo(size.width * 0.8328574, size.height * 0.6521855);
    path_1.lineTo(size.width * 0.8713593, size.height * 0.6508109);
    path_1.lineTo(size.width * 0.8929833, size.height * 0.6803655);
    path_1.lineTo(size.width * 0.9049222, size.height * 0.6449364);
    path_1.lineTo(size.width * 0.9417778, size.height * 0.6331109);
    path_1.lineTo(size.width * 0.9107500, size.height * 0.6125109);
    path_1.lineTo(size.width * 0.9118426, size.height * 0.5755491);
    path_1.lineTo(size.width * 0.8807000, size.height * 0.5983582);
    path_1.lineTo(size.width * 0.8446296, size.height * 0.5873836);
    path_1.close();

    Paint paint1Fill = Paint()..style = PaintingStyle.fill;
    paint1Fill.color = const Color(0xffCDCECE).withOpacity(1.0);
    path_1.fillType = PathFillType.evenOdd;
    canvas.drawPath(path_1, paint1Fill);

    Path path_2 = Path();
    path_2.moveTo(size.width * 0.2501593, size.height * 0.7266964);
    path_2.lineTo(size.width * 0.1757346, size.height * 0.7614455);
    path_2.lineTo(size.width * 0.1051443, size.height * 0.7212473);
    path_2.lineTo(size.width * 0.1145556, size.height * 0.8013091);
    path_2.lineTo(size.width * 0.05288815, size.height * 0.8552345);
    path_2.lineTo(size.width * 0.1330839, size.height * 0.8697145);
    path_2.lineTo(size.width * 0.1652970, size.height * 0.9432436);
    path_2.lineTo(size.width * 0.2056389, size.height * 0.8723109);
    path_2.lineTo(size.width * 0.2874204, size.height * 0.8636709);
    path_2.lineTo(size.width * 0.2319056, size.height * 0.8053255);
    path_2.lineTo(size.width * 0.2501593, size.height * 0.7266964);
    path_2.close();

    Paint paint2Fill = Paint()..style = PaintingStyle.fill;
    paint2Fill.color = const Color(0xffCDCECE).withOpacity(1.0);
    path_2.fillType = PathFillType.evenOdd;
    canvas.drawPath(path_2, paint2Fill);

    Path path_3 = Path();
    path_3.moveTo(size.width * 0.8637796, size.height * 0.3953018);
    path_3.lineTo(size.width * 0.6483463, size.height * 0.3646873);
    path_3.lineTo(size.width * 0.5515019, size.height * 0.1727120);
    path_3.lineTo(size.width * 0.4546574, size.height * 0.3646873);
    path_3.lineTo(size.width * 0.2392241, size.height * 0.3953018);
    path_3.lineTo(size.width * 0.3950704, size.height * 0.5439382);
    path_3.lineTo(size.width * 0.3581833, size.height * 0.7544564);
    path_3.lineTo(size.width * 0.5515019, size.height * 0.6550164);
    path_3.lineTo(size.width * 0.7448204, size.height * 0.7544564);
    path_3.lineTo(size.width * 0.7079333, size.height * 0.5439382);
    path_3.lineTo(size.width * 0.8637796, size.height * 0.3953018);
    path_3.close();

    Paint paint3Fill = Paint()..style = PaintingStyle.fill;
    paint3Fill.color = const Color(0xffA0A2A3).withOpacity(1.0);
    path_3.fillType = PathFillType.evenOdd;
    canvas.drawPath(path_3, paint3Fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
