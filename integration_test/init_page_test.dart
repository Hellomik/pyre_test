import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:qyre_test/main.dart' as app;

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  testWidgets("TEst scroll", (tester) async {
    app.main();
    await tester.pumpAndSettle();

    final scrollFind = find.byKey(const Key('main_scroll'));
    await tester.fling(scrollFind, const Offset(0, -500), 10);
    await tester.pumpAndSettle();
  });
}
